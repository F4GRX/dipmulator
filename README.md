# dipmulator

This is a modern 27256 EPROM emulator. It has the following features:

 * Only connected via I2C : can be programmed by any arduino or device
   supporting I2C. All components on the board support the 1 MHz high speed
 * Footprint is not larger than a DIP28 wide chip. No cumbersome ribbon wire
   or anything else is required, just plug this circuit in place of the EPROM
   and use the I2C bus to program it.
 * Has an on-board i2C EEPROM to hold a binary image for the on-board SRAM
 * In-circuit programmable: When programming happens, the emulating SRAM is
   disconnected from the system bus.

The compact design was achieved by routing the address and data lines in an
arbitrary way that does not follow the usual bus order. It does not matter at
all, since the data and addresses can be pre-mangled in software.
